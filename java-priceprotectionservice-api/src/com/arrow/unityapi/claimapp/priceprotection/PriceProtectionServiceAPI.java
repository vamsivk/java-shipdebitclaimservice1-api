package com.arrow.unityapi.claimapp.priceprotection;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.arrow.components.enterprise.foundation.ServiceManager;
import com.arrow.components.enterprise.log.Logger;
import com.arrow.utils.timing.TimerUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import com.google.gson.Gson;

public class PriceProtectionServiceAPI {
    
    private Gson gson;
    
    public PriceProtectionServiceAPI() {
        super();
    }
    
    public Response getUserResponsibilities(
                            @ApiParam(value = "The global id of the user", required = true) @QueryParam("globalid") String globalId,
                            @ApiParam(value = "The region to search for the user", required = true) @QueryParam("region") String region) {
                    TimerUtil totalTime = new TimerUtil();

                    UserRequest userReq = UserRequest.buildUserResponsibilityRequest(globalId,
                                    region);

                    // create the error response to be used
                    ErrorResponse errorResp = new ErrorResponse();
                    boolean hasError = false;

                    StringBuilder sb = new StringBuilder();
                    UserWrapper uWrapper = null;

                    if (userReq != null) {
                            if (userReq.isValid()) {
                                    logger.debug(this, userReq.toString());

                                    try {
                                            uWrapper = this.provider.execute(userReq);

                                            if (uWrapper != null) {
                                                    if (uWrapper.isError()) {
                                                            errorResp = uWrapper.getErrorResponse();
                                                            hasError = true;
                                                    }
                                                    else {
                                                            sb.append(uWrapper.getResponse(gson));
                                                    }
                                            }
                                            else {
                                                    errorResp.setReturnMsg("userWrapper was null");
                                                    hasError = true;
                                            }
                                    }
                                    catch (ServiceException se) {
                                            errorResp.setReturnMsg(se.getMessage());
                                            hasError = true;
                                    }

                                    // if we got here - this is a service error
                                    if (hasError) {
                                            sb.append(
                                                            ErrorResponse.formatErrorResponse(gson, errorResp));

                                            return Response
                                                            .status(Response.Status.INTERNAL_SERVER_ERROR)
                                                            .entity(sb.toString()).build();
                                    }
                            }
                            else {
                                    errorResp.setReturnMsg(userReq.getInvalidDetails());
                                    hasError = true;
                            }
                    }
                    else {
                            errorResp.setReturnMsg("invalid user request");
                            hasError = true;
                    }

                    // everything here is a request error
                    if (hasError) {
                            sb.append(ErrorResponse.formatErrorResponse(gson, errorResp));
                            return Response.status(Response.Status.BAD_REQUEST)
                                            .entity(sb.toString()).build();
                    }

                    totalTime.stop();
                    double elapsedTime = totalTime.getDiffPrecise(TimeUnit.MILLISECONDS);
                    logger.info(this, "request id: " + userReq.getId() + " *** total time: "
                                    + elapsedTime + " ms");

                    return Response.ok(sb.toString()).build();
            }
}
